require_relative "BattleShip"
class Engage
	def initialize *args
		@player1=args[0]
		@player2=args[1]

		player1_fire
	end
	def player1_fire
		#puts "===> player1#{ @player1.counter} <<<<<<<<<   #{@player1.set_target.length}"
		while @player1.counter < @player1.set_target.length
			#puts "player1_fire #{@player1.counter}"
			hit = false
			@player2.ship_location.each do|k|
				k.each do|a,s|
				 	hit_guess=s.select{|r| r.include?("#{@player1.set_target[@player1.counter]}")}
			 	 	if hit_guess.any?
			 	 		s.delete(hit_guess.join(""))
			 	 		#incomplete logic for different types ships
			 	 		# if a.downcase=="p"
			 	 		# 	s.delete(hit_guess)
			 	 		# 	#set_target[@player1.counter].delete(s)
			 	 		# elsif a.downcase=="q"
			 	 			
			 	 		# 	s[s.index(hit_guess[0])]="0"
			 	 			
			 	 		# end
			 	 		hit=true
			 	 	end
				end
				
			end
			print_message("Player1", @player1.set_target[@player1.counter],hit )
			#puts "----------#{@player2.ship_location}--------"
			@player1.counter=@player1.counter+1
		 	player2_fire unless hit


		end
		if(@player1.counter < @player1.set_target.length)==false
			@player1.counter=@player1.set_target.length
			puts "Player1 No More Missles Left"
		end
	end

	def player2_fire
		#puts "===> player2#{ @player2.counter} <<<<<<<<<   #{@player2.set_target.length}"
		while @player2.counter < @player2.set_target.length
			#puts "player2_fire #{i}"
			hit =false
			@player1.ship_location.each do|k|
				k.each do|a,s|

					hit_guess=s.select{|r| r.include?("#{@player2.set_target[@player2.counter]}")}
			 	 	if hit_guess.any?
			 	 		
			 	 		s.delete(hit_guess.join(""))
			 	 		#incomplete logic for different types ships
			 	 		# if a.downcase=="p"
			 	 		# 	s.delete(hit_guess)
			 	 		# 	#set_target[@player1.counter].delete(s)
			 	 		# elsif a.downcase=="q"
			 	 		# 	s[s.index(hit_guess[0])]="0"
			 	 		# end
			 	 		hit=true
			 	 	end
				end
			end
			#puts "----------#{@player1.ship_location}--------"
			print_message("Player2", @player2.set_target[@player2.counter],hit )		 
			@player2.counter=@player2.counter+1
			player1_fire unless hit
		end
		if(@player2.counter < @player2.set_target.length)==false
			puts "Player2 No More Missles Left"
			@player2.counter = @player2.set_target.length
		end
	end
	
	def print_message(player, target, attack)
		attack = attack == true ? "hit" : "missed"
		puts "#{player} fires a missile with target #{target} which #{attack}"
	end

end