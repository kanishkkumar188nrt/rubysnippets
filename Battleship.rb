require_relative "Engage.rb"
class BattleShip

	#attr_accessor :battle_ground, :set_target, :ship_location
	 LETTER_NUMBER_ARRAY=('a'..'z').to_a
	 Game=Struct.new(:battle_ground,:ship_location,  :set_target, :counter)
	def initialize(args)
			
			
		@input=args
		#puts args.inspect
		initialize_game
		Engage.new(player1_info, player2_info)
		#puts @player1.inspect
		decide_winner
		#hit_target
	end
	
	
	#Incomplete Winner
	def decide_winner
		puts @player1.ship_location.map{|ship_hash| ship_hash.any?{|k,v| v.empty?}}.inspect
		# puts @player1.inspect
		# puts"Player 2 is the winner" if @player1.ship_location.map{|ship_hash| ship_hash.any?{|k,v| v.empty?}}.length >= 2
		# puts  @player2.ship_location.map{|ship_hash| ship_hash.any?{|k,v| v.empty?}}.length #>= 2
	end
	
		def valid_location(battle,cordinates)
			#puts "#{battle[0]}==#{LETTER_NUMBER_ARRAY}=-=--=--=-=-=-=--=-#{LETTER_NUMBER_ARRAY.index(battle[0].downcase).to_i}---------------------------> #{cordinates[0].to_i < LETTER_NUMBER_ARRAY.index(battle[0].downcase).to_i}"
			return cordinates.join("") if ((cordinates[0].to_i || LETTER_NUMBER_ARRAY.index(battle[0].downcase).to_i+1) < LETTER_NUMBER_ARRAY.index(battle[0].downcase).to_i+1 && cordinates[1].to_i < battle[1].to_i)
		end

		def player1_info
			@player1
		end
		def player2_info
			@player2
		end
		def initialize_game

			@player1=Game.new
			@player2=Game.new
			@player1.counter=0
			@player2.counter=0

			@bg=Array.new(@input["player1"]["battleground"][0].to_i) {Array.new(@input["player1"]["battleground"][1].to_i)}
			
			@player1.battle_ground=@input["player1"]["battleground"]
			@player2.battle_ground=@input["player2"]["battleground"]
		
			@player1.ship_location=assign_ship(@input["player1"])
			@player2.ship_location=assign_ship(@input["player2"])
			@player1.set_target=@input["player1"]["targets"].map!{|i| (LETTER_NUMBER_ARRAY.index(i.split("")[0].downcase).to_i).to_s+i.split("")[1].to_s}
			@player2.set_target=@input["player2"]["targets"].map!{|i| (LETTER_NUMBER_ARRAY.index(i.split("")[0].downcase).to_i).to_s+i.split("")[1].to_s}

		end
		def assign_ship(input)
			arr2=[]
			input["ships"].map do|k,v|
				if v["size"][0].to_i < LETTER_NUMBER_ARRAY.index(@player1.battle_ground[0].downcase).to_i+1 && (v["size"][0].to_i-1+v["location"][0].to_i) < LETTER_NUMBER_ARRAY.index(@player1.battle_ground[0].downcase).to_i+1 && (v["size"][1].to_i-1)+v["location"][1].to_i < @player1.battle_ground[1].to_i &&v["size"][1].to_i < @player1.battle_ground[1].to_i
					ar=[]
					for i in LETTER_NUMBER_ARRAY.index(v["location"][0].downcase).to_i..(LETTER_NUMBER_ARRAY.index(v["location"][0].downcase).to_i+v["size"][1].to_i)-1
						for j in v["location"][1].to_i..(v["location"][1].to_i+v["size"][0].to_i)-1
							 ar << "#{i}#{j}"
						end	
					end
					arr2<<{"#{k}" => ar}
				else
					puts "Please Enter Valid Location for ship" 
					exit
				end
			end
			return arr2
		end


		def set_target
			self.set_target=@input["targets"]
		end
	
	
end
puts "\n<---------Let the game begin--------->"