class TryingDynamicMethods
  def method_missing(*args)
      singleton_class.class_eval { attr_accessor "#{args[0]}" }
      send("#{args[0]}=", args[1])
    #end
  end
end

params={name: "Kanishk", account:"123123", email:"kanishkkumar188nrt@gmail.com"}
t=TryingDynamicMethods.new
params.each do|key,value|
  t.send("#{key}", value)
end
p t.name