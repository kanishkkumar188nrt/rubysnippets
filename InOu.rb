#require_relative "Engage"
require_relative "BattleShip"
class InOu
	#include BattleShip
	PLAYERS=["player1","player2"]
	@@h=Hash.new {|h,k| h[k]=Hash.new {|v,y| v[y]=Hash.new{|a,b|a[b]=Hash.new}}}
	def initialize
		#puts self.method_list
		#Input for 2 players
		PLAYERS.each do|pl|
			self.method_list.map do|mthd|
				self.send(mthd.to_s+'=',pl)
			end
		end
	end
	# location_battleship
	def method_list
		pre_mn="collect_"
		method_cals=%w(battle_size battleship_type ship_size location_battleship target_locations).map{|i| (pre_mn+i).to_sym}
	end
	def collect_battle_size= *args
		#puts args
		puts "Enter Battle Ground matrix:"
		while @battleground_size = gets.chomp # loop while getting user input
		  case @battleground_size
		  when /^[a-zA-Z]{1}[\d]{1}$/
		  	@battleground_size=@battleground_size.split("")
		    break # make sure to break so you don't ask again
		  else
		    puts "Please enter valid Battle Ground Matrix, example A1 upto Z9"
		    print ">" # print the prompt, so the user knows to re-enter input
		  end
		end
	 	@@h[args]["battleground"]= @battleground_size
		 
	end
	
	def collect_battleship_type= *args
		#puts args
		puts "Enter BattleShip Type P/Q:"
		while @battle_ship_type = gets.chomp # loop while getting user input
		  case @battle_ship_type
		  when /^[P|Q]$/
		    break # make sure to break so you don't ask again
		  else
		    puts "Please enter valid BattleShip Type P or Q"
		    print ">" # print the prompt, so the user knows to re-enter input
		  end
		end
	end

	def collect_ship_size= *args
		puts "Enter Ship size:"
		while ship_size = gets.chomp # loop while getting user input
			#puts BattleShip.valid_location(battleground_size, ship_size_1.split(""))
		  case ship_size
		  when /^[\d]{2}$/ && BattleShip.valid_location(@battleground_size, ship_size.split(""))
		  	ship_size=ship_size.split("")
		    break # make sure to break so you don't ask again
		  else
		    puts "Please enter valid BattleShip Ship in double digit 11 upto 99"
		    print ">" # print the prompt, so the user knows to re-enter input
		  end
		end
		@@h[args]["ships"]["#{@battle_ship_type}"]["size"]=ship_size
	end
	
	

	def collect_location_battleship= *args
		puts "Location for BattleShip 1:"
		while loc_b=gets.chomp # loop while getting user input
			#raise BattleShip.valid_location(battleground_size, loc_b1.split("")).inspect
		  case loc_b

		  when /^[a-zA-Z][\d]$/ && BattleShip.valid_location(@battleground_size, loc_b.split(""))
		  	loc_b1=loc_b.split("")
		    break # make sure to break so you don't ask again
		  else
		    puts "Please enter valid location for BattleShip 1, example A1 upto Z9"
		    print ">" # print the prompt, so the user knows to re-enter input
		  end
		end
		@@h[args]["ships"]["#{@battle_ship_type}"]["location"]=loc_b
	end
	def collect_target_locations= *args
		puts "Enter comma seperated coordinates for Player 1:"
		#target_a=gets.chomp.split(",")

		while target_a=gets.chomp
		 	case target_a+","
		  when /^([a-zA-Z]\d{1},)+$/
		  	target_a=target_a.split(",")
		    break # make sure to break so you don't ask again
		  else
		    puts "Please enter valid location for BattleShip 1, example A1 ,D3,F4"
		    print ">" # print the prompt, so the user knows to re-enter input
		  end
		end
		@@h[args]["targets"]= target_a
	end


	def input_hash; @@h; end;

end


#io=InOu.new
#puts io.input_hash.inspect
h=h={"player1"=>{"battleground"=>["E", "5"], "ships"=>{"P"=>{"size"=>["1", "1"], "location"=>"23"}}, "targets"=>["A1", "A3", "B2", "B3"]}, "player2"=>{"battleground"=>["E", "5"], "ships"=>{"Q"=>{"size"=>["2", "2"], "location"=>"33"}}, "targets"=>["B1", "B2", "A3", "A4", "D4"]}}
BattleShip.new(h)

##
#{"[\"player1\"]"=>{"battleground"=>["E", "5"], "ships"=>{"P"=>{"size"=>["1", "1"], "location"=>"23"}}, "targets"=>["A1", "A2", "A3"]}, "[\"player2\"]"=>{"battleground"=>["E", "5"], "ships"=>{"Q"=>{"size"=>["2", "3"], "location"=>"22"}}, "targets"=>["C2", "C3"]}}



